import pirc
import interface
import curses
import re
import quizbot
import threading
import sched
import time
from functools import partial

# Host and port
HOST = "irc.patwic.com"
PORT = 6667

# IRC server password
PASSWORD = "foobar"

# Create IRC server connection object
irc = pirc.IRC(HOST, PORT, PASSWORD)

# User data
NICK = "QuizBot"
REALNAME = "The allmighty"

# Connect to server and register user
irc.connect(NICK, REALNAME)

# Channel
CHAN = "#patwic"

# Join channel
irc.send("JOIN " + CHAN)

DEFAULT_COLOR = interface.YELLOW

ANSWER_TIME = 30

#QUESTIONS = {}

OPS = ["Simko", "frry"]


def get_questions(filename):
    questions = {}
    with open(filename) as f:
        for line in f:
            question, answer = line.split("[]")
            questions[question.strip()] = answer.strip()
    return questions
        

def parse_usr_input(inp, ui):
    try:
        to_print = "[" + NICK + "]"
        color = None
        if inp.startswith("@"):
            color = interface.BLUE
            too = inp[1:inp.find(" ")]
            msg = inp[inp.find(" ") + 1:]
            to_print += "(PRIVMSG "+  too + ") " + msg
            irc.send("PRIVMSG " + too + " :" + msg)
        elif inp.startswith("/"):
            to_print = "[COMMAND] " + inp[1:]
            color = interface.GREEN
            irc.send(inp[1:])
        else:
            to_print += "(PRIVMSG " + CHAN + ") " + inp
            color = interface.CYAN
            irc.send("PRIVMSG " + CHAN + " :" + inp)
        ui.set_text_color(color)
        ui.output(to_print)
        ui.set_text_color(DEFAULT_COLOR)
    except IndexError:
        ui.set_text_color(interface.RED)
        ui.ouput("INVALID SYNTAX")
        ui.set_text_color(DEFAULT_COLOR)
    
def parse_server_msg(msg, ui, quizbot):
    span = re.match(r":(\w)*!~", msg)
    
    if span is not None:
        span = span.span(0)
        usr_name = msg[1:span[1] - 2]
    else:
        usr_name = "SERVER"
    first_space = msg.find(" ")
    second_space = msg[first_space +1:].find(" ") + first_space + 1
    msg_type = msg[first_space + 1:second_space]
    additional_info = ""
    if msg_type == "PRIVMSG":
        to = ""
        ind = second_space + 1
        #ui.output(msg[second_space:])
        while msg[ind] != " ":
            to += msg[ind]
            ind += 1
        additional_info += " " + to
        second_space = msg[second_space + 2:].find(" ") + second_space + 2
        if usr_name in OPS and msg[second_space + 2:].startswith("START") and not quizbot.started:
            start_game(ui, quizbot)
        elif usr_name in OPS and msg[second_space + 2:].startswith("OP "):
            real_msg = msg[second_space + 2:]
            OPS.append(real_msg[len("OP "):])
        quizbot.msgHandler(usr_name, additional_info[1:], msg[second_space + 2:])
        #ui.output(msg[second_space + 2:])
    to_print = "[" + usr_name + "]" + "(" + msg_type +  additional_info + ") " + msg[second_space + 1:]
    ui.set_text_color(DEFAULT_COLOR)
    ui.output(to_print)
    
def start_game(ui, quizbot):
    parse_usr_input("Nu börjar ett nytt quiz!!! Alla är med och tävlar", ui)
    time.sleep(1)
    parse_usr_input("Ni kommer ha %d sekunder på er att svara på frågan" %(ANSWER_TIME), ui)
    time.sleep(1)
    parse_usr_input("Den som svarar först (och rätt) på frågan får 3 poäng, alla andra får 1", ui)
    time.sleep(1)
    parse_usr_input("Nu kommer första frågan:", ui)
    time.sleep(5)
    parse_usr_input(quizbot.newQuestion(), ui)
    quizbot.started = True
    
    part = partial(new_question, ui, quizbot)
    timer = sched.scheduler(time.time, time.sleep)
    timer.enter(ANSWER_TIME, 1, part)
    next_call = threading.Thread(target=timer.run)
    next_call.start()
    

def new_question(ui, quizbot):
    parse_usr_input("Tiden är nu slut. Rätt svar var: %s" %(quizbot.questions[quizbot.curr_question]), ui)
    next_question = quizbot.newQuestion()
    if next_question is not None:
        parse_usr_input("Nästa fråga är: %s" %(next_question), ui)
        
        part = partial(new_question, ui, quizbot)
        
        timer = sched.scheduler(time.time, time.sleep)
        timer.enter(ANSWER_TIME, 1, part)
        next_call = threading.Thread(target=timer.run)
        next_call.start()
    else:
        parse_usr_input("Nu är quizet slut", ui)
        parse_usr_input("Den slutgiltiga ställningen blev följande:", ui)
        quizbot.msgHandler("Root", "#patwic", "SCORE")
        quizbot.reset()
    

def main(stdscreen):

    ui = interface.TextUI(stdscreen)
    ui.set_text_color(interface.RED)
    ui.output("ENTER PASSWORD") #PASS FOR BOT: quiz4life
    ui.set_text_color(DEFAULT_COLOR)
    
    bot = quizbot.QuizBot(ui, irc, get_questions("questions.txt"), NICK, CHAN)
    
    msg = ui.input()
    while not msg:
        msg = ui.input()
    irc.send("PRIVMSG NickServ :identify " + msg)

    while True:
        msg = irc.read()
        if msg:
            parse_server_msg(msg, ui, bot)
            #ui.output(msg)
        usr_msg = ui.input()
        if usr_msg:
            parse_usr_input(usr_msg, ui)

curses.wrapper(main)
