import random

class QuizBot:
    def __init__(self, ui, irc, questions, bot_name, chan_name):
        self.ui = ui
        self.irc = irc
        self.bot_name = bot_name
        self.chan_name = chan_name
        
        self.scores = {}
        self.questions = questions
        self.asked = []
        self.first = True
        self.curr_question = ""
        self.started = False
        #print(questions, file=open("lot.txt", "w"))
    
    def reset(self):
        self.scores = {}
        self.asked = []
        self.first = True
        self.started = False
        
    def getScore(self, person):
        return scores.get(person, 0)
    
    def _getRandQuestion(self):
        keys = self.questions.keys()
        ind_to_pick = random.randint(0, len(keys) - 1)
        for ind, val in enumerate(keys):
            if ind == ind_to_pick:
                return val
    
    def newQuestion(self):
        if len(self.asked) == len(self.questions):
            return None
        question = self._getRandQuestion()
        while question in self.asked:
            question = self._getRandQuestion()
        self.asked.append(question)
        self.curr_question = question
        self.first = True
        return question
    
    def getRanking(self):
        return [name[0] for name in sorted(list(self.scores.items()), key=lambda x: x[1])]
    
    def answerQuestion(self, usr, answer):
        if self.curr_question != "":
            if answer.lower().strip() == self.questions[self.curr_question].lower():
                if self.first:
                    self.scores[usr] = self.scores.get(usr, 0) + 3
                    self.first = False
                else:
                    self.scores[usr] = self.scores.get(usr, 0) + 1
            score = self.scores.get(usr, 0)
            rank = self.getRanking().index(usr) + 1 if usr in self.getRanking() else len(self.scores) + 1
            return score, rank
            
    def send_mutiline(self, to, msg):
        lines = msg.split("\n")
        for line in lines:
            self.irc.send("PRIVMSG %s :%s" %(to, line))
    
    def msgHandler(self, usr, to, msg):
        if msg.startswith("ANSWER "):
            prev_score = self.scores.get(usr, 0)
            score, rank = self.answerQuestion(usr, ' '.join(msg.split()[1:]))
            if score != prev_score:
                self.irc.send("PRIVMSG %s :Snyggt! Din nya poäng är: %d vilket gör att du ligger på plats: %d" %(usr, score, rank))
            else:
                self.irc.send("PRIVMSG %s :Tyvärr, det var fels svar. Förök igen" %(usr))
        if msg.startswith("HELP"):
            help_msg = """Här är alla saker du kan skriva till mig:
HELP\t\tVisar detta hjälpmedelande
  
ANSWER\t\tAnvänds för att skicka in ett svar. Svaret kan
 \t\t\tvara ett privat medelande eller ett vanlig i kanalen
   
SCORE\t\tSkriver ut den nuvarande poängställningen"""
            if to == self.bot_name:
                self.send_mutiline(usr, help_msg)
            else:
                self.send_mutiline(self.chan_name, help_msg)
        if msg.startswith("SCORE"):
            score_msg = '\n'.join(["%s:\t%d" %(name, self.scores[name]) for name in self.getRanking()])
            if to == self.bot_name:
                self.send_mutiline(usr, score_msg)
            else:
                #print(self.chan_name, score_msg, file=open("log.txt", "a"))
                #self.irc.send(self.chan_name, score_msg)
                self.send_mutiline(self.chan_name, score_msg)
