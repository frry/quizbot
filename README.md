# IRC - Internet Relay Chat

IRC är ett *protokoll* för att skicka meddelanden över Internet. En server tar
emot meddelandet och tolkar det.

    PASS foobar
    NICK monty
    USER monty irc.patwic.com nil :Montezuma Xocoyotzin
    JOIN #patwic


Du är nu inne i kanalen `patwic` på servern `irc.patwic.com`. Din
## 4. Skriv ut skickade meddelanden i gränssnittet

Tillgängliga färger:

* BLUE 
* CYAN
* GREEN
* MAGENTA 
* RED
* WHITE 
* YELLOW

Tex:

    ui.set_text_color(interface.BLUE)
